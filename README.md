# SQL implementation in console application c#


## Description
This application was part of an assignment related to an introduction to sql usage in C#. The application implements functions to fetch, add and manipulate the data that is stored on a specified SQL database. 

## Usage
To test the functions, see function calls (example: testUpdateCustomer) in the program.cs file. The functions are commented out by default. The functions to fetch information from the used SQL database is present in the CustomerRepository file.



## Authors
Johan Lantz([johan.lantz.a](https://gitlab.com/johan.lantz.a)) Jeremy Matthiessen(@Jerry585)

## Contributing
Pull requests are welcome. For major changes, please open an issue to discuss what you would change.
