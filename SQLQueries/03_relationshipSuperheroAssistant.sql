USE SuperheroDb

ALTER TABLE Assistant
	ADD HeroID int FOREIGN KEY REFERENCES Superhero(ID)
