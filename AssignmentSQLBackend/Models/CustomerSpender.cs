﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentSQLBackend.Models
{
    public class CustomerSpender
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public decimal total { get; set; }
    }
}
