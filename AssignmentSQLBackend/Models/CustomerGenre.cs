﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentSQLBackend.Models
{
    public class CustomerGenre
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string genre { get; set; }
        public int num { get; set; }

    }
}
