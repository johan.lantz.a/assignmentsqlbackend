﻿
using Microsoft.Data.SqlClient;
using AssignmentSQLBackend.Models;
using AssignmentSQLBackend.Repositories;



ICustomerRepository repository = new CustomerRepository();

// Test for requirement one!
//testSelectAll(repository);


// Test for requirement two!
//testSelect(repository);

//test for requirement three!
//testSelectByName(repository);


//test for requirement four!
//PrintCustomers(repository.GetPageOfCustomer(5, 10));


//test for requirement five!
Customer customer = new()
{
    FirstName = "Test3",
    LastName = "Testar",
    Country = "Sweden",
    PostalCode = "139 68",
    Phone = "076 123 456 78",
    Email = "t.testar@test.com"
};
//repository.CreateNewCustomer(customer);


//Test for requirement six!
//testUpdateCustomer(repository);


// test for requirement seven!
//testCountriesAndAmmount(repository);


//test for requirement eight!
//testCustomerSpender(repository);


//test fore requirement nine!
//testCustomerGenre(repository);




static void testSelectAll(ICustomerRepository repository)
{
    PrintCustomers(repository.GetAllCustomers());
}

static void testSelect(ICustomerRepository repository)
{
    PrintCustomer(repository.GetSpecificCustomer(1));
}

static void testSelectByName(ICustomerRepository repository)
{
    PrintCustomers(repository.GetSpecificCustomer("H"));
}

static void testCountriesAndAmmount(ICustomerRepository repository)
{
    PrintCountriesAndAmmount(repository.GetCustomerByCountry());
}

static void testCustomerSpender(ICustomerRepository repository)
{
    printCustomerSpenders(repository.GetCustomerBySpending());
}

static void testCustomerGenre(ICustomerRepository repository)
{
    printCustomerGenres(repository.GetCustomerGenres(1));
}




static void testInsert(ICustomerRepository repository)
{
    Customer test = new Customer()
    {
        FirstName = "ThisIsMyFirstName",
        LastName = "ThisIsMySurname",
        Email = "testytester@tester.com"
    };
    if (repository.CreateNewCustomer(test))
    {
        Console.WriteLine("New Customer added");
    }
    else
    {
        Console.WriteLine("Could not create new customer");
    }
}


//Requirement six
static void testUpdateCustomer(ICustomerRepository repository)
{
    Customer customer = new Customer()
    {
        CustomerId = 1,
        FirstName = "newname",
        LastName = "Something",
        Company = "testföretag",
        Address = "Theodor-Heuss-Straße 34",
        City = "Uppsala",
        State = "testState",
        Country = "Sweden",
        PostalCode = "12345",
        Phone = "+41 123456",
        Fax = "FaxNrOrSomething",
        Email = "PetersNewMail@mail.com",
        SupportRepId = 3
    };
    if (repository.UpdateCustomer(customer))
    {
        Console.WriteLine("Update message from program");
    }
    else
    {
        Console.WriteLine("Something went wrong in program?");
    }
}


static void PrintCountriesAndAmmount(IEnumerable<CountryCustomers> countryCustomers )
{
    foreach (CountryCustomers oneCountryAndAmmount in countryCustomers)
    {
        PrintCountryAndAmmount(oneCountryAndAmmount);
    }
}


static void PrintCountryAndAmmount(CountryCustomers countryCustomers)
{
    Console.WriteLine($"----{countryCustomers.country} {countryCustomers.ammount}");
}



static void PrintCustomers(IEnumerable<Customer> customers)
{
    foreach (Customer customer in customers)
    {
        PrintCustomer(customer);
    }
}

static void PrintCustomer(Customer customer)
{
    Console.WriteLine($"----{customer.CustomerId} {customer.FirstName} {customer.LastName} Country: {customer.Country} Postal code: {customer.PostalCode} Phone: {customer.Phone} Email: {customer.Email} ");
}



static void printCustomerSpenders(IEnumerable<CustomerSpender> customerSpenders)
{
    foreach (CustomerSpender customerSpender in customerSpenders)
    {
        printCustomerSpender(customerSpender);
    }
}


static void printCustomerSpender(CustomerSpender customerSpender)
{
    Console.WriteLine($"----{customerSpender.firstName} {customerSpender.lastName} with total: {customerSpender.total}");
}


static void printCustomerGenres(IEnumerable<CustomerGenre> customerGenres)
{
    foreach (CustomerGenre customerGenre in customerGenres)
    {
        printCustomerGenre(customerGenre);
    }
}


static void printCustomerGenre(CustomerGenre customerGenre)
{
    Console.WriteLine($"----{customerGenre.firstName} {customerGenre.lastName} {customerGenre.genre} {customerGenre.num}");
}