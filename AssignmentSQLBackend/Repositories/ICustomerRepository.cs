﻿using AssignmentSQLBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentSQLBackend.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetSpecificCustomer(int id);

        public List<Customer> GetSpecificCustomer(string name);

        public bool CreateNewCustomer(Customer customer);

        public List<Customer> GetAllCustomers();

        public List<Customer> GetPageOfCustomer(int limit, int offset);

        public bool UpdateCustomer(Customer customer);

        public List<CountryCustomers> GetCustomerByCountry();

        public List<CustomerSpender> GetCustomerBySpending();

        public List<CustomerGenre> GetCustomerGenres(int id);
    }
}
