﻿using Microsoft.Data.SqlClient;
using AssignmentSQLBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentSQLBackend.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Adds a new <see cref="Customer"/> to the database.
        /// </summary>
        /// <param name="customer">The <see cref="Customer"/> that should be added.</param>
        /// <returns>true if the customer is successfully added, else false.</returns>
        public bool CreateNewCustomer(Customer customer)
        {
            bool finished = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);

                        finished = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                throw ex;
            }

            return finished;
        }
        /// <summary>
        /// Queries the sql database for all CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
        /// and returns them in a list.
        /// </summary>
        /// <returns>A list of <see cref="Customer"/> with all it's data.</returns>
        /// <exception cref="SqlException"></exception>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                

                               

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }                               
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Something went wrong");
                Console.WriteLine(ex.Message);
            }
                    return customerList;
        }

        /// <summary>
        /// queries the sql database for country aswell as counts, groups and orders them descending.Results in a list
        /// of countries from which the customers are from, aswell as how many customers comes from each country.
        /// </summary>
        /// <returns>A list of <see cref="CountryCustomers"/> with all it's data.</returns>
        /// <exception cref="SqlException"></exception>
        public List<CountryCustomers> GetCustomerByCountry()
        {
            List<CountryCustomers> customersByCounry = new List<CountryCustomers>();
            string sql = "SELECT Country, Count(Country) FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql,connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CountryCustomers temp = new CountryCustomers();
                                temp.country = reader.GetString(0);
                                temp.ammount = reader.GetInt32(1);
                                
                                customersByCounry.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Something went wrong int CustomerRepository");
                Console.WriteLine(ex.Message);
            }

            return customersByCounry;
        }



        /// <summary>
        /// Gets the customer firstname, lastname and invoice totals and orders them descending from the invoice total.
        /// The result is stored in a list of customerspenders which i returned at the end of function.
        /// </summary>
        /// <returns>A list of <see cref="CustomerSpender"/> with all it's data.</returns>
        /// <exception cref="SqlException"></exception>
        public List<CustomerSpender> GetCustomerBySpending()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();
            string sql = "SELECT Invoice.Total, Customer.FirstName, Customer.LastName FROM Invoice INNER JOIN Customer ON Invoice.CUstomerId=Customer.CustomerId " +
                "ORDER BY Invoice.total DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql,connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.total = reader.GetDecimal(0);
                                temp.firstName = reader.GetString(1);
                                temp.lastName = reader.GetString(2);

                                customerSpenders.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Something went wrong in CustomerSPending in CustomerRepository");
                Console.WriteLine(ex.Message);
            }
            return customerSpenders;
        }

        /// <summary>
        /// Gets a specific customer based on the id.
        /// </summary>
        /// <param name="id">The customer id.</param>
        /// <returns>A <see cref="Customer"/> with all it's data.</returns>
        /// <exception cref="SqlException"></exception>
        public Customer GetSpecificCustomer(int id)
        {
            Customer customer = new();
            string sql = "SELECT * FROM Customer WHERE CustomerId = @id";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.Company = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.Address = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.City = reader.GetString(5);
                                if (!reader.IsDBNull(6)) customer.State = reader.GetString(6);
                                if (!reader.IsDBNull(7)) customer.Country = reader.GetString(7);
                                if (!reader.IsDBNull(8)) customer.PostalCode = reader.GetString(8);
                                if (!reader.IsDBNull(9)) customer.Phone = reader.GetString(9);
                                if (!reader.IsDBNull(10)) customer.Fax = reader.GetString(10);
                                customer.Email = reader.GetString(11);
                                if (!reader.IsDBNull(12)) customer.SupportRepId = reader.GetInt32(12);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return customer;
        }

        /// <summary>
        /// Gets one or more customers starting with the specified name.
        /// </summary>
        /// <param name="name">The customers name or starting letters.</param>
        /// <returns>A <see cref="List{T}"/> with all customers with the specified name.</returns>
        /// <exception cref="SqlException"></exception>
        public List<Customer> GetSpecificCustomer(string name)
        {
            List<Customer> customers = new();
            string sql = "SELECT * FROM Customer WHERE FirstName LIKE @FirstName";

            try
            {
                using(SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", name + "%");
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer tmpCustomer = new();
                                
                                tmpCustomer.CustomerId = reader.GetInt32(0);
                                tmpCustomer.FirstName = reader.GetString(1);
                                tmpCustomer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) tmpCustomer.Company = reader.GetString(3);
                                if (!reader.IsDBNull(4)) tmpCustomer.Address = reader.GetString(4);
                                if (!reader.IsDBNull(5)) tmpCustomer.City = reader.GetString(5);
                                if (!reader.IsDBNull(6)) tmpCustomer.State = reader.GetString(6);
                                if (!reader.IsDBNull(7)) tmpCustomer.Country = reader.GetString(7);
                                if (!reader.IsDBNull(8)) tmpCustomer.PostalCode = reader.GetString(8);
                                if (!reader.IsDBNull(9)) tmpCustomer.Phone = reader.GetString(9);
                                if (!reader.IsDBNull(10)) tmpCustomer.Fax = reader.GetString(10);
                                tmpCustomer.Email = reader.GetString(11);
                                if (!reader.IsDBNull(12)) tmpCustomer.SupportRepId = reader.GetInt32(12);

                                customers.Add(tmpCustomer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                throw ex;
            }

            return customers;
        }

      

        /// <summary>
        /// Gets all customers within a given range.
        /// </summary>
        /// <param name="limit">The limit for how many customers should be fetched.</param>
        /// <param name="offset">The offest from where to start fetching.</param>
        /// <returns>A <see cref="List{T}"/> of all customers within the specified range.</returns>
        /// <exception cref="SqlException"></exception>
        public List<Customer> GetPageOfCustomer(int limit, int offset)
        {
            List<Customer> customers = new();
            string sql = "SELECT * FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";

            try
            {
                using (SqlConnection connection = new(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Offset", offset);
                        command.Parameters.AddWithValue("@Limit", limit);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer tmpCustomer = new();

                                tmpCustomer.CustomerId = reader.GetInt32(0);
                                tmpCustomer.FirstName = reader.GetString(1);
                                tmpCustomer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) tmpCustomer.Company = reader.GetString(3);
                                if (!reader.IsDBNull(4)) tmpCustomer.Address = reader.GetString(4);
                                if (!reader.IsDBNull(5)) tmpCustomer.City = reader.GetString(5);
                                if (!reader.IsDBNull(6)) tmpCustomer.State = reader.GetString(6);
                                if (!reader.IsDBNull(7)) tmpCustomer.Country = reader.GetString(7);
                                if (!reader.IsDBNull(8)) tmpCustomer.PostalCode = reader.GetString(8);
                                if (!reader.IsDBNull(9)) tmpCustomer.Phone = reader.GetString(9);
                                if (!reader.IsDBNull(10)) tmpCustomer.Fax = reader.GetString(10);
                                tmpCustomer.Email = reader.GetString(11);
                                if (!reader.IsDBNull(12)) tmpCustomer.SupportRepId = reader.GetInt32(12);

                                customers.Add(tmpCustomer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }


            return customers;
        }


        /// <summary>
        /// Takes a customer with all required information as input and updates the sql database where the CustomerId
        /// matches between the database and the input customer. 
        /// </summary>
        /// <param name="customer"></param>
        /// <returns> Returns a bool of true if function sucessfully updated a customer on the sql database. Returns 
        /// false if something went wrong. </returns>
        /// <exception cref="SqlException"></exception>
        public bool UpdateCustomer(Customer customer)
        {
        bool finished = false;

        string sql = "UPDATE Customer " +
            "SET FirstName = @FirstName, LastName = @LastName, Company = @Company, Address = @Address, " +
            "City = @City, State = @State, Country = @Country, PostalCode = @PostalCode, " +
            "Phone = @Phone, Fax = @Fax, Email = @Email, SupportRepId = @SupportRepId " +
            "WHERE CustomerId = @CustomerId";

        try
        {
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);

                    if (customer.FirstName != null) cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                    if (customer.LastName != null) cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                    if (customer.Company != null) cmd.Parameters.AddWithValue("@Company", customer.Company);
                    if (customer.Address != null) cmd.Parameters.AddWithValue("@Address", customer.Address);
                    if (customer.City != null) cmd.Parameters.AddWithValue("@City", customer.City);
                    if (customer.State != null) cmd.Parameters.AddWithValue("@State", customer.State);
                    if (customer.Country != null) cmd.Parameters.AddWithValue("@Country", customer.Country);
                    if (customer.PostalCode != null) cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                    if (customer.Phone != null) cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                    if (customer.Fax != null) cmd.Parameters.AddWithValue("@Fax", customer.Fax);
                    if (customer.Email != null) cmd.Parameters.AddWithValue("@Email", customer.Email);
                    if (customer.SupportRepId != null) cmd.Parameters.AddWithValue("@SupportRepId", customer.SupportRepId);
                    finished = cmd.ExecuteNonQuery() > 0 ? true : false;
                }
            }
        }
        catch (SqlException ex)
        {
            Console.WriteLine("something went wrong");
            Console.WriteLine(ex);
        }
        Console.WriteLine("Updated Customer");
        return finished;
    }

        /// <summary>
        /// Queries the sql database for information on from Customer table and joins a path to connect customer to
        /// genre for music connected to the customers. The information fetched from the sql database is grouped
        /// and ordered as to result in a list where a specified customer is shown with its most relevant music genre
        /// first in the list. The result is saved in a temporary list (findBiggest) where the first element
        /// (which will be the largest thanks to sql order) is compared to the following elements in the list 
        /// to see if there is a tie for most common music genre for that customer. The elements that matches the value
        /// of the first element, aswell as the first element is added the the final list that is returned.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of <see cref="CustomerGenre"/> with all it's data.</returns>
        /// <exception cref="SqlException"></exception>
        public List<CustomerGenre> GetCustomerGenres(int id)
        {
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();
            List<CustomerGenre> findBiggest = new List<CustomerGenre>();
            string sql = "SELECT Customer.FirstName, Customer.LastName, Genre.Name, Count(Genre.Name) as Num " +
                "FROM Customer INNER JOIN Invoice ON Customer.CustomerId=Invoice.CustomerId " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId=InvoiceLine.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId=Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId=Genre.GenreId " +
                "WHERE Customer.CustomerId = @id " +
                "GROUP BY Customer.FirstName, Customer.LastName, Genre.Name " +
                "ORDER BY COUNT(Genre.Name) DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@id",id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.firstName = reader.GetString(0);
                                temp.lastName = reader.GetString(1);
                                temp.genre = reader.GetString(2);
                                temp.num = reader.GetInt32(3);

                                findBiggest.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Something went wrong in customerGenre");
                Console.WriteLine(ex.Message);
            }
            int tempInt = findBiggest[0].num;
            foreach (CustomerGenre item in findBiggest)
            {
                if (item.num >= tempInt)
                {
                    customerGenres.Add(item);
                }
            }
            return customerGenres;
        }
    }
}
